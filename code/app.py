from flask import Flask
from flask_restful import Resource, Api
from item.item import Item

app = Flask(__name__)
api = Api(app)

items = []


api.add_resource(Item, '/item/<string:name>')

app.run(port=5000)