from flask_restful import Resource, Api
import sys
sys.path.insert(1, '/Users/connordodge/code/learning-practice/udemy/flask/flask-restful/code/item_list')
from item_list import ItemList

class Item(Resource):
    
    def get(self, name):
        response_obj = {'item': None}
        status_code = 404
        items_with_name = list(filter(lambda item: item['name'] == name, ItemList.get_items()))
        response_obj, status_code = (items_with_name[0], 200) if len(items_with_name) > 0 else (response_obj, status_code)
        return response_obj, status_code
    
    def post(self, name):
        item = {'name': name, 'price': '12.00'}
        ItemList.append_item(item)
        return item, 201