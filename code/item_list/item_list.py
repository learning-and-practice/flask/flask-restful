from flask_restful import Resource, Api

class ItemList(Resource):
    items=[]

    def get_items(self):
        return self.items
    def append_item(self, item):
        self.items.append(item)